require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/puma'
require 'mina/rbenv'
require 'mina/whenever/tasks'
require 'mina_sidekiq/tasks'
require 'yaml'

YAML.load(File.open('config/deploy.yml')).keys.each do |server|

  def load_config(server)
    yaml_config = YAML.load(File.open('config/deploy.yml'))
    
    puts "-----> Set domain to #{server} server"
    set :domain, yaml_config[server]['domain']
    set :rails_env, yaml_config[server]['rails_env']
    set :deploy_to, yaml_config[server]['deploy_to']
    set :branch, yaml_config[server]['branch']
    set :user, yaml_config[server]['user']

    puts "[DOMAIN] #{fetch(:domain)}"
  end

  desc %{Set up #{server} for deployment}
  task "setup_#{server}" => :environment do
    load_config(server)
    invoke :setup
  end

  desc %{deploy to #{server} server}
  task "deploy_#{server}" => :environment do
    load_config(server)
    invoke :deploy
  end

  desc %{run puma start to #{server} server}
  task "puma_start_#{server}" => :environment do
    load_config(server)
    invoke :'puma:start'
  end

  desc %{run puma stop to #{server} server}
  task "puma_stop_#{server}" => :environment do
    load_config(server)
    invoke :'puma:stop'
  end

  desc %{run puma restart to #{server} server}
  task "puma_restart_#{server}" => :environment do
    load_config(server)
    invoke :'puma:restart'
  end
end

set :repository, 'git@gitlab.com:alamtopani/shortener.git'
set :keep_releases, 5
set :shared_dirs, fetch(:shared_dirs, []).push('log', 'tmp/pids', 'tmp/sockets', 'public/uploads')
set :shared_files, fetch(:shared_files, []).push('config/database.yml', 'config/secrets.yml')
set :forward_agent, true

task :environment do
  invoke :'rbenv:load'
end

task :setup => :environment do
  command %[mkdir -p "#{fetch(:shared_path)}/log"]
  command %[chmod g+rx,u+rwx "#{fetch(:shared_path)}/log"]

  command %[mkdir -p "#{fetch(:shared_path)}/config"]
  command %[chmod g+rx,u+rwx "#{fetch(:shared_path)}/config"]

  command %(mkdir -p "#{fetch(:shared_path)}/tmp/sockets")
  command %(chmod g+rx,u+rwx "#{fetch(:shared_path)}/tmp/sockets")

  command %(mkdir -p "#{fetch(:shared_path)}/tmp/pids")
  command %(chmod g+rx,u+rwx "#{fetch(:shared_path)}/tmp/pids")

  command %[touch "#{fetch(:shared_path)}/config/database.yml"]
  command %[touch "#{fetch(:shared_path)}/config/mongoid.yml"]
  command %[touch "#{fetch(:shared_path)}/config/secrets.yml"]
  # command %[touch "#{fetch(:shared_path)}/config/puma.rb"]
  comment  %[echo "-----> Be sure to edit '#{fetch(:shared_path)}/config/database.yml', 'mongoid.yml', 'secrets.yml'."]

  if fetch(:repository)
    repo_host = fetch(:repository).split(%r{@|://}).last.split(%r{:|\/}).first
    repo_port = /:([0-9]+)/.match(fetch(:repository)) && /:([0-9]+)/.match(fetch(:repository))[1] || '22'

    comment %[
      if ! ssh-keygen -H  -F #{repo_host} &>/dev/null; then
        ssh-keyscan -t rsa -p #{repo_port} -H #{repo_host} >> ~/.ssh/known_hosts
      fi
    ]
  end
end

desc "Deploys the current version to the server."
task :deploy => :environment do

  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'
    invoke :'puma:restart'
  end
end

