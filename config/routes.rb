Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'public#home'
  resources :shorteneds
  get "/:short_url", to: 'shorteneds#show'
  post "/api/shorteneds/create", to: 'api/shorteneds#create', as: 'create_api_shorteneds'
end
