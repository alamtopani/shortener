class ShortenedVisit < ApplicationRecord
  belongs_to :shortened, optional: true

  scope :top, ->{order(click: :desc)}
end
