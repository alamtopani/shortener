class Shortened < ApplicationRecord
  has_many :shortened_visits

  UNIQ_CODE_LENGTH = 6
  validates :original_url, presence: true, on: :create
  validates_format_of :original_url, :with => /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?\Z/i
  before_create :generate_short_url

  def find_duplicate
    Shortened.find_by_sanitize_url(self.sanitize_url)
  end

  def generate_short_url
    url = ([*('a'..'z'), *('0'..'9')]).sample(UNIQ_CODE_LENGTH).join.upcase
    exist_url = Shortened.find_by_short_url(url)
    if exist_url.present?
      self.generate_short_url
    else
      self.short_url = url
    end
  end

  def sanitize
    self.sanitize_url = self.original_url.downcase.gsub(/(https?:\/\/)|(www\.)/, "")
    self.sanitize_url = "http://#{self.sanitize_url}"
  end

  def update_visit_url(request)
    # locale = Timeout::timeout(5) { Net::HTTP.get_response(URI.parse('http://api.hostip.info/country.php?ip=' + request.location.ip )).body } rescue "ID"
    visit = self.shortened_visits.find_or_create_by(shortened_id: self.id, country: request.location.country_code)
    visit.click = visit.click+1
    visit.save
  end

end
