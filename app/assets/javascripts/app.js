$(document).ready(function(){
  $("#loader-wrapper").fadeOut(2000, function(){
  });

  $(document).on('click','.copy-link', function() {
    var copyText = $(this).siblings('.value-link');
    copyText.select();
    document.execCommand("copy");
    alert("Copied the text: " + copyText.val());
  });
});