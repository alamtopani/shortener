class PublicController < ApplicationController
  def home
    if params[:original_url].present?
      sanitize_url = params[:original_url].downcase.gsub(/(https?:\/\/)|(www\.)/, "")
      sanitize_url = "http://#{sanitize_url}"
      @url = Shortened.find_by_sanitize_url(sanitize_url)
    end
  end
end