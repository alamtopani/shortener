class Api::ShortenedsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    @url = Shortened.new
    @url.original_url = params[:original_url]
    @url.sanitize
    if @url.find_duplicate.present?
      render json: {url: "#{root_url + @url.find_duplicate.short_url}", mesage: 'Short links for this link have been created!'}, status: 200
    else
      if @url.save
        render json: {url: "#{root_url + @url.short_url}", mesage: 'A short link for this link was successfully created!'}, status: 200
      else
        render json: {mesage: @url.errors.full_messages.join('')}, status: :unprocessable_entity
      end
    end
  end

end