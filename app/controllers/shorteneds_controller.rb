class ShortenedsController < ApplicationController
  def show
    @url = Shortened.find_by_short_url(params[:short_url])
    if @url.present?
      @url.update_visit_url(request)
      redirect_to @url.sanitize_url  
    else
      redirect_to root_url
    end
  end

  def create
    @url = Shortened.new
    @url.original_url = params[:original_url]
    @url.sanitize
    if @url.find_duplicate.present?
      redirect_to root_url(original_url: params[:original_url]), notice: 'Short links for this link have been created!'
    else
      if @url.save
        redirect_to root_url(original_url: params[:original_url]), notice: 'A short link for this link was successfully created!'
      else
        redirect_to root_url, alert: @url.errors.full_messages.join('')
      end
    end
  end

end