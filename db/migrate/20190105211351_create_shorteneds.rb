class CreateShorteneds < ActiveRecord::Migration[5.1]
  def change
    create_table :shorteneds do |t|
      t.string :original_url
      t.string :short_url
      t.string :sanitize_url

      t.timestamps
    end
  end
end
