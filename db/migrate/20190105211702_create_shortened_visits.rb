class CreateShortenedVisits < ActiveRecord::Migration[5.1]
  def change
    create_table :shortened_visits do |t|
      t.integer :shortened_id
      t.string :country
      t.integer :click, default: 0

      t.timestamps
    end

    add_index :shortened_visits, :shortened_id
  end
end
